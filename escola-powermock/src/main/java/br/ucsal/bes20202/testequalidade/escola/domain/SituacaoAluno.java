package br.ucsal.bes20202.testequalidade.escola.domain;

public enum SituacaoAluno {
	ATIVO, CANCELADO;
}
