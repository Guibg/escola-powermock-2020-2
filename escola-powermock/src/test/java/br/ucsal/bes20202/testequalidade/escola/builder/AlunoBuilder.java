package br.ucsal.bes20202.testequalidade.escola.builder;

import br.ucsal.bes20202.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20202.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {

	private static final Integer MATRICULA = 1;
	private static final String NOME = "Pedro Santos";
	private static final SituacaoAluno SITUACAO = SituacaoAluno.ATIVO;
	private static final Integer ANO_NASCIMENTO = 2000;

	private Integer matricula = MATRICULA;
	private String nome = NOME;
	private SituacaoAluno situacao = SITUACAO;
	private Integer anoNascimento = ANO_NASCIMENTO;

	private AlunoBuilder() {

	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder comSitucacao(SituacaoAluno situacao) {
		this.situacao = situacao;
		return this;
	}

	public AlunoBuilder ativo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder cancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}

	public AlunoBuilder nascidoEm(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder mas() {
		return new AlunoBuilder().comNome(nome).comMatricula(matricula).comSitucacao(situacao).nascidoEm(anoNascimento);
	}
	
	
	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setSituacao(situacao);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}
}
