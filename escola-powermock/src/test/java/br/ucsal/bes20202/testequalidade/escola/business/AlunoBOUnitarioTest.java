package br.ucsal.bes20202.testequalidade.escola.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20202.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20202.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20202.testequalidade.escola.persistence.AlunoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AlunoBO.class, AlunoDAO.class })
public class AlunoBOUnitarioTest {

	/**
	 * 
	 * Caso de teste 1: Verificar se alunos ativos são salvos.
	 * 
	 * Entrada: uma instância de aluno ativo;
	 * 
	 * Saída esperada: como este método é void, para avaliação seu comportamento
	 * você deve verificar se houve chamada ao método salvar da classe AlunoDAO.
	 *
	 * Obs1: lembre-se de que o teste é unitário, ou seja, você deve mocar a classe
	 * AlunoDAO.
	 * 
	 * Obs2: NÃO é necessário mocar o método getSituacao().
	 *
	 */
	@Test
	public void testarSalvamentoAlunoAtivo() {

		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno();
		Aluno aluno1 = alunoBuilder.build();

		AlunoBO alunoBO = new AlunoBO();
		AlunoDAO alunoDAO = new AlunoDAO();
		AlunoBO spy1 = PowerMockito.spy(alunoBO);
		AlunoDAO spy2 = PowerMockito.spy(alunoDAO);
		
		PowerMockito.mockStatic(AlunoBO.class);
		PowerMockito.mockStatic(AlunoDAO.class);

		spy1.salvar(aluno1);

		PowerMockito.verifyStatic(AlunoDAO.class);
		AlunoDAO.salvar(aluno1);
		
		
		//verify(spy1, times(1)).salvar();
	}

}
